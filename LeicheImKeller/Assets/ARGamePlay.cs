﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ARGamePlay : MonoBehaviour {


    private static string roomName;
    private static string playerName;
    private static bool isComplete = false;
    int ARTime =0;

    private static List<Card> cardList;

    void Start()
    {
        cardList = new List<Card>();

    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            DebugcreateTimeCard();
        }
    }

    public static void sendGameObject(GameObject element)
    {
        string parentName = element.transform.parent.name;
        Debug.Log("ParentName:" + parentName);
        
        switch(parentName)
        {

           case "Player":
               getPlayerName(element);
               break;

           case "Card":
               getCard(element);
               Debug.Log("Card!");
               break;

           case "Room":
               getRoom(element);
               Debug.Log("Room!");
               break;
       }

    }

    private static void getPlayerName(GameObject element)
    {
        TrackableBehaviour handler = element.GetComponent<TrackableBehaviour>();
        playerName = handler.name.Replace("Player_", "");
    }

    private static void getCard(GameObject element)
    {
        TrackableBehaviour handler = element.GetComponent<TrackableBehaviour>();
        string cardName= handler.name.Replace("Action_", "");

        if(cardName.Equals("TimeCard_20"))
        {
            Debug.Log("TIME20");
            Card timeCard = new Card(CardType.TimeCard);
            timeCard.timeScore = 20;
            cardList.Add(timeCard);
            sendToGameManager();
        }

        else if(cardName.Equals("TimeCard_30"))
        {

            Debug.Log("TIME30");
            Card timeCard = new Card(CardType.TimeCard);
            timeCard.timeScore = 30;
            cardList.Add(timeCard);
            sendToGameManager();
        }

        else if (cardName.Equals("TimeCard_10"))
        {

            Debug.Log("TIME10");
            Card timeCard = new Card(CardType.TimeCard);
            timeCard.timeScore = 10;
            cardList.Add(timeCard);
            sendToGameManager();
        }

        else
        {
            Card actionCard = new Card(CardType.ActionCard);
            actionCard.effect = (CardEffect)Enum.Parse(typeof(CardEffect), cardName);
            cardList.Add(actionCard);
        }
    }

    private void DebugcreateTimeCard()
    {
        Card timeCard = new Card(CardType.TimeCard);
        timeCard.timeScore = 30;
        cardList.Add(timeCard);
        sendToGameManager();
    }

    private static void sendToGameManager()
    {
       
       
        PlayerName name = (PlayerName)Enum.Parse(typeof(PlayerName), playerName);
        Card[] cardStack = cardList.ToArray();
        PlayerMove newMove = new PlayerMove(cardStack,name);

        CGameController.startNewMove(newMove);
    }


    private static void getRoom(GameObject element)
    {
        TrackableBehaviour handler = element.GetComponent<TrackableBehaviour>();
        roomName = handler.name.Replace("Room_", "");
        Card roomCard = new Card(CardType.RoomCard);
        roomCard.room = (Room)Enum.Parse(typeof(Room), roomName);
        cardList.Add(roomCard);
    }

}

