﻿using UnityEngine;
using System.Collections;

public class Drunk : MonoBehaviour {

	public BlurEffect leftBlurEffect;
	public BlurEffect rightBlurEffect;

	private const float minBlurEffect = -1.7f;
	private const float maxBlurEffect = 0.5f;
	
	// minimal and maximal time after which blurriness changes when drunk
	private const float minBlurChangeTime = 1;
	private const float maxBlurChangeTime = 7;
	private float blurChangeTime;
	private float newBlurValue;
	private float oldBlurValue;
	
	private float lastChangeTime;

	// Use this for initialization
	void Start () {
		leftBlurEffect.enabled = true;
		rightBlurEffect.enabled = true;
		oldBlurValue = leftBlurEffect.blurSpread;
		blurChangeTime = Random.Range(minBlurChangeTime, maxBlurChangeTime);
		newBlurValue = Random.Range(minBlurEffect, maxBlurEffect);
		lastChangeTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - lastChangeTime > blurChangeTime) {
			oldBlurValue = leftBlurEffect.blurSpread;
			blurChangeTime = Random.Range(minBlurChangeTime, maxBlurChangeTime);
			newBlurValue = Random.Range(minBlurEffect, maxBlurEffect);
			lastChangeTime = Time.time;
		}
		leftBlurEffect.blurSpread = Mathf.Lerp(oldBlurValue, newBlurValue, (Time.time - lastChangeTime) / blurChangeTime);
		rightBlurEffect.blurSpread = Mathf.Lerp(oldBlurValue, newBlurValue, (Time.time - lastChangeTime) / blurChangeTime);
	}
}
