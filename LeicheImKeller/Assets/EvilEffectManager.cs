﻿using UnityEngine;
using System.Collections;


public class EvilEffectManager : MonoBehaviour {

	public Camera leftEye;
	public Camera rightEye;

	public PoisonEffect poisonEffect;
	public LSD lsdEffect;
	public Drunk drunkenEffect;

    /// <summary>
    /// Activate the fancy Element
    /// </summary>
    /// <param name="effectEnum"></param>
    /// <returns></returns>
	
    
    public void setEvilEffectActive(CardEffect effectEnum)
    {
		switch (effectEnum) {
            //Drunk
            case CardEffect.Drunk:
				drunkenEffect.enabled = true;
				break;
			case CardEffect.Poison:
				poisonEffect.enabled = true;
				break;
			case CardEffect.LSD:
				lsdEffect.enabled = true;
				break;
		}
    }

}
