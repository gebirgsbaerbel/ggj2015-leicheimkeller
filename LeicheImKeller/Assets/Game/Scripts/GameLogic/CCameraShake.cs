﻿using UnityEngine;
using System.Collections;

public class CCameraShake : MonoBehaviour
{

    Vector3 originPosition = new Vector3();
    Quaternion originRotation = new Quaternion();
    public float shake_decay = 0.002f;
    public float shake_intensity = 0.3f;

    void OnGUI()
    {
        if (GUI.Button(new Rect(20, 40, 80, 20), "Shake"))
        {
            Shake();
        }
    }
    void Update()
    {
        if (shake_intensity > 0)
        {
            transform.position = originPosition + Random.insideUnitSphere * shake_intensity;
            transform.rotation = new Quaternion(
            originRotation.x + Random.Range(-shake_intensity, shake_intensity) * .2f,
            originRotation.y + Random.Range(-shake_intensity, shake_intensity) * .2f,
            originRotation.z + Random.Range(-shake_intensity, shake_intensity) * .2f,
            originRotation.w + Random.Range(-shake_intensity, shake_intensity) * .2f);
            shake_intensity -= shake_decay;
        }
    }
    void Shake()
    {
        originPosition = transform.position;
        originRotation = transform.rotation;
    }
}