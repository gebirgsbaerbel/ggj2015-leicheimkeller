﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class CGameController
{
    public static bool bInitialized { get; private set; }

    // Player Move Data
    public static PlayerMove playerMove { get; private set; }
    
    // Player General Data
    public static int iPlayerAmount { get; private set; }
    public static int iClueAmountPerPlayer { get; private set; }
    public static int iClueAmountTotal { get; private set; }
    

    private static Dictionary<Room, List<CItemSceneData>> itemSceneDataDictionaryList = new Dictionary<Room, List<CItemSceneData>>();
    private static int iItemLimitPerRoom;


    



    public static void initialize(int _iPlayerAmount, int _iClueAmount)
    {
        bInitialized = true;

        iPlayerAmount = _iPlayerAmount;
        iClueAmountPerPlayer = _iClueAmount;

        iClueAmountTotal = iClueAmountPerPlayer * iPlayerAmount;

        // Initiaize the Dic(k) Lists
        for (int i = 1; i < 5; i++)
		{
            itemSceneDataDictionaryList.Add((Room)i, new List<CItemSceneData>());
		}

        initItemData();
    }

    public static void startNewMove(PlayerMove _move)
    {
        playerMove = _move;

        Application.LoadLevel((int)_move.selectedRoom);
    }

    // Calculate all Item Data
    private static void initItemData()
    {
        int iItemsPlaced = 0;

        for (int i = 0; i < iPlayerAmount; i++)
        {
            int iRoomIterator = 1;
            // Place items in Room
            while (iItemsPlaced < (i + 1) * iClueAmountPerPlayer)
            {
                Room room = (Room)iRoomIterator;
                
                if(itemSceneDataDictionaryList[room].Count < 9 /*Max Items per Room*/)
                    setItem((PlayerName)i, room);

                iRoomIterator = iRoomIterator + 1 < 5 ? iRoomIterator + 1 : 1;
                iItemsPlaced++;
            }
        }
    }

    private static void setItem(PlayerName name, Room room)
    {
        int ID = Random.Range(0, 9);

        while (true)
        {
            if (itemSceneDataDictionaryList[room].Any(x => x.ID == ID))
                ID = Random.Range(0, 9);
            else
                break;
        }
        
        itemSceneDataDictionaryList[room].Add(new CItemSceneData(name, ID));

        Debug.LogWarning("PlayerName:" + name + " Room: " + room + " ID:" + ID);
    }

    // Calculate the Item Data (Room, Against Player, id -> Position)
    public static void setItemData(ref List<CItem> itemsInScene)
    {
        Stack<CItem> foundItems = new Stack<CItem>();

        foreach(CItem item in itemsInScene)
        {
            CItemSceneData itemData = itemSceneDataDictionaryList[playerMove.selectedRoom].Find(x => x.ID == item.ID);

            if (itemData == null) continue;

            if (itemData.bFound)
                foundItems.Push(item);
            else
            {
                item.eProveAgainstPlayer = itemData.eProofAgainstPlayer;

                item.itemDataRef = itemData;
            }
        }

        while(foundItems.Count > 0)
        {
            CItem item = foundItems.Pop();
            GameObject go = item.gameObject;
            itemsInScene.Remove(item);

            GameObject.Destroy(go);
        }
    }
}
