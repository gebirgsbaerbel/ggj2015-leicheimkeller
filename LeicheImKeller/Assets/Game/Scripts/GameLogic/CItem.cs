﻿using UnityEngine;
using System.Collections;

public class CItem : MonoBehaviour
{
    public CollectableItems item;
    public int ID;
    public enum EItemFocusState { none, far, close };
    public EItemFocusState eItemFocusState
    {
        get;
        set;
    }

    public PlayerName eProveAgainstPlayer;

    // Needed so we can mark this itemdata ref as found when focusing item
    public CItemSceneData itemDataRef
    {
        get;
        set;
    }
}

public enum CollectableItems
{
    Messer, 
    Schere, 
    Puempel, 
    Rasierer, 
    Topfpflanze
}

