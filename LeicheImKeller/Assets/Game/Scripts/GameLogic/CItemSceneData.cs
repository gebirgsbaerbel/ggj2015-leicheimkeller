﻿using UnityEngine;
using System.Collections;

public class CItemSceneData
{
    public bool bFound;
    public PlayerName eProofAgainstPlayer;
    public int ID;

    public CItemSceneData(PlayerName _ePlayer, int _ID)
    {
        bFound = false;
        eProofAgainstPlayer = _ePlayer;
        ID = _ID;
    }
}
