﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CObjectController : MonoBehaviour
{
    public Camera m_camera = null;
    public Transform m_transCamera = null;
    public CObjectEffectController objEffect = null;

    public float m_fFocusTimeTarget = 2.0F;

    public float fFocusScreenRangeClose = 0.1F;
    public float fFocusScreenRangeFar = 0.2F;

    public AudioSource foundSound = null;
    
    public List<CItemSceneData> collectedItems = new List<CItemSceneData>();

    private float m_fFocusTimeCurrent = 0.0F;
    private CItem m_currentItem = null;
    

    private List<CItem> m_li_allItems;
    private Vector2 v2ScreenMid
    {
        get
        {
            return new Vector2(0.5F, 0.5F);
        }
    }


    // Use this for initialization
    void Start()
    {
        m_li_allItems = FindObjectsOfType<CItem>().ToList();

        CGameController.setItemData(ref m_li_allItems);
    }

    // Update is called once per frame
    void Update()
    {
        //checkItemsInRange();
        checkItemsInRange();

        updateFocusTime();
    }

    private void checkItemsInRange()
    {
        foreach (CItem item in m_li_allItems)
        {
            float fDistanceOnScreen = fGetDistanceToScreenMid(item);

            // Check if close enough to be highlighted
            if (fDistanceOnScreen < fFocusScreenRangeFar)
            {
                setItemFar(item, fDistanceOnScreen);

                // Check if close enough to be focused
                if (fDistanceOnScreen < fFocusScreenRangeClose)
                {
                    setItemClose(item, fDistanceOnScreen);
                }
                // Still in far
                else if (m_currentItem == item && m_currentItem && m_currentItem.eItemFocusState == CItem.EItemFocusState.close)
                {
                    // Go back to far State
                    m_currentItem.eItemFocusState = CItem.EItemFocusState.far;

                    Debug.Log("End Focus on Item");

                    if (objEffect.gameObject.activeSelf)
                        objEffect.gameObject.SetActive(false);
                }
            }
            // Nothing at all
            else if (m_currentItem == item && m_currentItem && m_currentItem.eItemFocusState == CItem.EItemFocusState.far)
            {
                // Go back to none State
                m_currentItem.eItemFocusState = CItem.EItemFocusState.none;

                if (objEffect.gameObject.activeSelf)
                    objEffect.gameObject.SetActive(false);

                m_currentItem = null;

                Debug.Log("End searching for Item");
            }
        }
    }

    private void setItemFar(CItem _item, float _fDistance)
    {
        // If currently focused is not null and not the Item -> set new
        if (m_currentItem != _item)
        {
            // If an item is set check if new one is closer
            if (!m_currentItem || (m_currentItem && _fDistance < fGetDistanceToScreenMid(m_currentItem)))
            {
                m_currentItem = _item;

                m_currentItem.eItemFocusState = CItem.EItemFocusState.far;

                Debug.Log("Start searching for Item");
            }
        }
    }

    private void setItemClose(CItem _item, float _fDistance)
    {
        if (m_currentItem.eItemFocusState != CItem.EItemFocusState.close)
        {
            m_fFocusTimeCurrent = 0.0F;

            m_currentItem.eItemFocusState = CItem.EItemFocusState.close;

            Debug.Log("Started Focus on Item");
        }
    }

    private void updateFocusTime()
    {
        if (!m_currentItem) return;

        if (m_currentItem.eItemFocusState == CItem.EItemFocusState.close)
        {
            m_fFocusTimeCurrent += Time.deltaTime;

            if (m_fFocusTimeCurrent < m_fFocusTimeTarget)
            {
                // Update Item Focus Close
                updateItemFocus(m_currentItem.eItemFocusState, fGetDistanceToScreenMid(m_currentItem));
            }
            // Focused long enough to collect Item
            else
            {
                pickupItem();
            }
        }
        else if(m_currentItem.eItemFocusState == CItem.EItemFocusState.far)
        {
            updateItemFocus(m_currentItem.eItemFocusState, fGetDistanceToScreenMid(m_currentItem));
        }
    }

    private void updateItemFocus(CItem.EItemFocusState eItemFocusState, float _fDistance)
    {
        if (!objEffect.gameObject.activeSelf)
            objEffect.gameObject.SetActive(true);

        // We don't need this Focus State here man!
        objEffect.update(fFocusScreenRangeClose, fFocusScreenRangeFar, _fDistance);
    }

    private void pickupItem()
    {
        // Update the Item Data Reference
        if (m_currentItem.itemDataRef != null)
        {
            m_currentItem.itemDataRef.bFound = true;

            collectedItems.Add(m_currentItem.itemDataRef);
        }

        // Remove from Items list
        m_li_allItems.Remove(m_currentItem);

        GameObject go = m_currentItem.gameObject;
        // Destroy the physical Scene Object
        Destroy(go);

        // Disable Focus Effect
        if (objEffect.gameObject.activeSelf)
            objEffect.gameObject.SetActive(false);

        if(foundSound)
            foundSound.Play();

        Debug.Log("Interaction with Item");

        m_currentItem = null;
    }

    private float fGetDistanceToScreenMid(CItem _item)
    {
        Vector3 v3ItemPos = m_camera.WorldToScreenPoint(_item.transform.position);

        Vector2 v2ItemPosScreen = new Vector2(v3ItemPos.x / Screen.width, v3ItemPos.y / Screen.height);

        return Vector2.Distance(v2ScreenMid, v2ItemPosScreen);
    }

}
