﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CObjectEffectController : MonoBehaviour
{
    public Color col1;
    public Color col2;
    public UnityEngine.UI.Image findEffect;

    public void update(float _boarder, float _max, float _fDistance)
    {
        if (_fDistance < _boarder)
            findEffect.color = col2;
        else
            findEffect.color = Color.Lerp(col1, col2, 1 - ((_fDistance - _boarder) / _boarder));
    }
}
