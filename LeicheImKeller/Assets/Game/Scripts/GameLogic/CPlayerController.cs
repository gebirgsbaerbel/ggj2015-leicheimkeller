﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CPlayerController : MonoBehaviour
{


    public LayerMask m_layerMaskItemTrigger = 0;
    
    [SerializeField]
    public Camera m_camera = null;

    [SerializeField]
    public Transform m_transCamera = null;


    public const float FocusTimeWTarget = 2.0f;
    public const float FocusScreenRange = 2.0f;
    public const float initWaitingTime = 3.0f; 

    private float m_fFocusTimeCurrent = 0.0F;
    private CItem m_currentlyFocusedItem = null;
    private List<CItem> allItemsList;


    //Effectmanager
    private EvilEffectManager evilManager;

	/// <summary>
	/// Init the Effects for the Game and start the Game
	/// </summary>
    void Start ()
    {


        //Find the Effectmanager
        evilManager = GetComponent<EvilEffectManager>(); 
        
        //Find all ItemObjects in a list
        allItemsList = FindObjectsOfType<CItem>().ToList();

        //Init the GameStats
        readTheInitFile();

        StartCoroutine(waitForRestart());
	}

    /// <summary>
    /// create the Evil effects for the Match
    /// </summary>
    private void readTheInitFile()
    {
        foreach (var element in Enum.GetValues(typeof(CardEffect)))
        {
            foreach (CardEffect arrayElement in CGameController.playerMove.effectList)
            {
                if (element.Equals(arrayElement))
                {
                    evilManager.setEvilEffectActive(arrayElement);
                }
            }
        }
    }


    IEnumerator waitForRestart()
    {
        yield return new WaitForSeconds(CGameController.playerMove.AR_Time);
        GetComponent<UpdateScreen>().UpdateTextField();
        yield return new WaitForSeconds(4);
        Application.LoadLevel("AppStart");
    }
}
