﻿using UnityEngine;
using System.Collections;

public class Card {

    public CardType type { get; set; }
    public CardEffect effect { get; set; }
    public int timeScore =0;
    public Room room;

    public Card(CardType type)
    {
        this.type = type;
        timeScore = 0; 
    }

    public Card (CardType type, int time)
    {
        this.type = type;
        this.timeScore = time; 
    }

    public Card(CardType type, Room roomInfo)
    {
        this.type = type;
        room = roomInfo; 
    }


    public Room getRoom()
    {
        return room;
    }

    public int getTimeScore()
    {
        return timeScore; 
    }
}

public enum CardType
{
    TimeCard, 
    ActionCard,
    RoomCard
}


public enum CardEffect
{
    None, 
    FindItem,
    Drunk,
	LSD,
	Poison
}

public enum Room
{
    None,
    Kitchen, 
    Bathroom, 
    Sleepingroom, 
    Livingroom, 
    Basement, 
    Attic
}

public enum PlayerName
{
    BigDaddy,
    CrazyTwin01,
    CrazyTwin02,
    RichBitch01,
    RichBitch02,
    weedhead,
    Chiller
}