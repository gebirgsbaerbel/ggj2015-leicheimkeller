﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayerMove {

    public int AR_Time;
    public CardEffect[] effectList;
    public Room selectedRoom;
    public PlayerName player;

    public PlayerMove(Card[] actionstack,PlayerName player)
    {

        List<CardEffect> effects = new List<CardEffect>() ;

        foreach(Card card in actionstack)
        {
            //Calculate the TimeEffects
            AR_Time += getTimeEffects(card);

            //Collect all effects of the Actioncards
            CardEffect effect = getEffect(card);

            if(effect != CardEffect.None)
            {
                effects.Add(effect);
            }

            effectList = effects.ToArray();


            //Get the Selected Room
            Room roomElement = getRoom(card);
            if(roomElement != Room.None)
            {
                selectedRoom = roomElement;
            }
        }        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="card"></param>
    /// <returns></returns>
    private int getTimeEffects(Card card)
    {
        if(card.type == CardType.TimeCard)
        {
            return card.getTimeScore();
        }
        return 0;
    }

    /// <summary>
    /// returns the Cardeffect only, if it is a ActionCarf
    /// </summary>
    /// <param name="card"></param>
    /// <returns></returns>
    private CardEffect getEffect(Card card)
    {
        if(card.type == CardType.ActionCard)
        {
            return card.effect;
        }

        return CardEffect.None;
    }

    /// <summary>
    /// Return the RoomCard
    /// </summary>
    /// <param name="card"></param>
    /// <returns></returns>
    private Room getRoom(Card card)
    {
        if (card.type == CardType.RoomCard)
        {
            return card.getRoom();
        }
        return Room.None;
    }
}

