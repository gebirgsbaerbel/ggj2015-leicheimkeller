﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestReadQRCode : MonoBehaviour,ITrackableEventHandler {


    private TrackableBehaviour mTrackingTarget;
    private bool isRenderingActive; 


    void Update()
    {
        Debug.Log("IsDetected: " + mTrackingTarget.TrackableName);

        GetComponent<Text>().text = "";
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if(newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED)
        {
            Debug.Log("IsDetected: "+mTrackingTarget.TrackableName);
        }
        else if(newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("IsDetected: " + mTrackingTarget.TrackableName);
        }
    
    }

    


}
