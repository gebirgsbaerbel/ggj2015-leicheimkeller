﻿using UnityEngine;
using System.Collections;

public class LSD : MonoBehaviour {

	public GlowEffect leftGlow;
	public GlowEffect rightGlow;

	public int lsdTime = 15;

	private float minHue = 0;
	private float maxHue = 1;

	private float lastChangeTime;

	private bool increaseHue = true;

	// Use this for initialization
	void Start () {
		leftGlow.enabled = true;
		rightGlow.enabled = true;

		lastChangeTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		float timeSpent = Time.time - lastChangeTime;
		Color currentColor = leftGlow.glowTint;
		HSBColor newColor = new HSBColor(currentColor);
		if (increaseHue) {
			newColor.h = Mathf.Lerp (minHue, maxHue, timeSpent / lsdTime);
		} else {
			newColor.h = Mathf.Lerp (maxHue, minHue, timeSpent / lsdTime);
		}
		leftGlow.glowTint = newColor.ToColor ();
		rightGlow.glowTint = newColor.ToColor ();
		if (timeSpent > lsdTime) {
			increaseHue = !increaseHue;
			lastChangeTime = Time.time;
		}
	}
}
