﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    int playerCount = 2;
    int hintCount = 5;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setPlayers(int playCount)
    {
        playerCount = playCount; 
    }

    public void setHintCount(int hitCount)
    {
        hintCount = hitCount;
    }

    public void startGameButton()
    {
        CGameController.initialize(playerCount,hintCount);
        Application.LoadLevel("BasicScene");
    }
}
