﻿using UnityEngine;
using System.Collections;

public class ObjectFinder : MonoBehaviour {

	void Update () {

        DoingRayCasting();
	}


    /// <summary>
    /// 
    /// </summary>
    private void DoingRayCasting()
    {
        Vector2 startPosition = Input.mousePosition;

        Ray camRay = Camera.main.ScreenPointToRay(startPosition);
        RaycastHit hit;

        if(Physics.Raycast(camRay,out hit, 15000f))
        {
            Debug.Log("ObjectName: " + hit.collider.gameObject.name);
        }
    }
}
