﻿using UnityEngine;
using System.Collections;

public class PoisonEffect : MonoBehaviour {

	public TwirlEffect leftTwirlEffect;
	public ScreenOverlay leftScreenOverlay;

	public TwirlEffect rightTwirlEffect;
	public ScreenOverlay rightScreenOverlay;
	
	public int twistTime = 4;

	private int minPoisonTwist = -50;
	private int maxPoisonTwist = 50;
	
	private bool moveRight = true;

	private float lastChangeTime;

	// Use this for initialization
	void Start () {
		leftScreenOverlay.enabled = true;
		rightScreenOverlay.enabled = true;
		leftTwirlEffect.enabled = true;
		rightTwirlEffect.enabled = true;

		leftTwirlEffect.angle = minPoisonTwist;
		rightTwirlEffect.angle = minPoisonTwist;
		lastChangeTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		float timeSpent = Time.time - lastChangeTime;
		if (moveRight) {
			leftTwirlEffect.angle = Mathf.Lerp(minPoisonTwist, maxPoisonTwist, timeSpent / twistTime);
			rightTwirlEffect.angle = Mathf.Lerp(minPoisonTwist, maxPoisonTwist, timeSpent / twistTime);
		} else {
			leftTwirlEffect.angle = Mathf.Lerp(maxPoisonTwist, minPoisonTwist, timeSpent / twistTime);
			rightTwirlEffect.angle = Mathf.Lerp(maxPoisonTwist, minPoisonTwist, timeSpent / twistTime);
		}
		if (timeSpent > twistTime) {
			moveRight = !moveRight;
			lastChangeTime = Time.time;
		}
	}
}
