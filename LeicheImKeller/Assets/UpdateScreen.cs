﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Linq; 

public class UpdateScreen : MonoBehaviour {


    public Text console;

    void Start () {
        console.text = "";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void UpdateTextField()
    {
        CItemSceneData[] collection = gameObject.GetComponent<CObjectController>().collectedItems.ToArray();
        string message = "";
        for(int i = 0; i < CGameController.iPlayerAmount; i++)
        {
            message += "You found " + collection.Count(x => x.eProofAgainstPlayer == (PlayerName)i)
                    + " hint(s) against "
                    + (PlayerName)i;
            
        }
        console.text = message;
    }
}
